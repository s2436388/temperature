package assignment1celsius;

public class Temperature {
    /**
     * Temperature Conversion
     *
     * @param celsius celsius
     * @return fahrenheit value
     */
    double celsiusToFahrenheit(String celsius) {
        return (Double.parseDouble(celsius) * 9/5) + 32;
    }

}
