package assignment1celsius;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.*;
import javax.servlet.http.*;

public class Celsius extends HttpServlet {

    private static final long serialVersionUID = 1L;
    private Temperature temperature;

    public void init() throws ServletException {
        temperature = new Temperature();
    }

    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        String docType =
                "<!DOCTYPE HTML>\n";
        String title = "Temperature Converter";
        out.println(docType +
                "<HTML>\n" +
                "<HEAD><TITLE>" + title + "</TITLE>" +
                "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
                "</HEAD>\n" +
                "<BODY BGCOLOR=\"green\">\n" +
                "<H1>" + title + "</H1>\n" +
                "  <P>Celsius: " +
                request.getParameter("celsius") + "\n" +
                "  <P>Fahrenheit: " +
                Double.toString(temperature.celsiusToFahrenheit(request.getParameter("celsius"))) +
                "</BODY></HTML>");
    }

}

